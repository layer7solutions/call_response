import sys
import re
import time
from difflib import SequenceMatcher
import configparser
import logging
import logging.config
import requests
from requests.exceptions import Timeout
import praw
import prawcore
import urllib3
from layer7_utilities import oAuth, LoggerConfig
from _version import __version__

config = configparser.ConfigParser()
config.read('botconfig.ini')

__author__ = "/u/D0cR3d, /u/thirdegree"
__botname__ = "CallReponse"
__description__ = "Responds to specific text with a response"
__dsn__ = config.get('BotConfig', 'DSN')
__logpath__ = config.get("BotConfig", "logpath")

_dbusername = config.get('Database', 'Username')
_dbpassword = config.get('Database', 'Password')
_dbhost = config.get('Database', 'Host')
_dbname = config.get('Database', 'DatabaseName')

APIKey = config.get('app', 'apikey')
APIUrl = "https://www.bungie.net/Platform/Destiny2/"
SUBREDDIT = "DestinyTheGame"


class CallResponse():
    def __init__(self):
        self.matchstring = r"\[\[([^[]+?)\]\]"  # [[searchquery]]
        self.linkprefix = "https://light.gg/db/items/"

        self.done = []
        self.starttime = time.time()

        # rate limiting variables
        self.lastquery = time.time()
        self.queryCount = 0
        self.ratelimitPerSec = 12  # n requests per second

        #################### LOGGER SETUP ############################
        # Setup logging
        config = LoggerConfig(__dsn__, __botname__, __version__, __logpath__)
        logging.config.dictConfig(config.get_config())
        self.logger = logging.getLogger('root')
        self.logger.info(u"/*********Starting App*********\\")
        self.logger.info(u"App Name: {} | Version: {}".format(
            __botname__, __version__))
        ################################################################

        self.canLaunch = self.login()
        self.subreddit = self.r.subreddit(SUBREDDIT)
        self.mods = self.subreddit.moderator()

        self.build_database()

    def login(self):
        try:
            self.logger.debug(u"/*********Getting Accounts*********\\")
            auth = oAuth()
            auth.get_accounts("dtgbot", __description__, __version__, __author__,
                              __botname__, _dbusername, _dbpassword, _dbhost, _dbname)
            for account in auth.accounts:
                self.r = account.login()
            self.logger.info(
                'Connected to account: {}'.format(self.r.user.me()))
            return True
        except:
            self.logger.error('Failed to log in.')
            return False

    def build_database(self):
        self.logger.info("Getting Manifest")
        r = requests.get("{}/Manifest/".format(APIUrl),
                         headers={'X-API-Key': APIKey})
        if r.status_code != 200:
            raise Exception("Request failed: {}".format(r.text))
        json = r.json()['Response']

        manifestURL = json['jsonWorldContentPaths']['en']
        manifest = requests.get(
            "https://www.bungie.net/{}".format(manifestURL), headers={'X-API-Key': APIKey})
        if manifest.status_code != 200:
            raise Exception("Manifest Request failed: {}".format(r.text))
        manifest = manifest.json()
        if 'DestinyInventoryItemDefinition' not in manifest:
            raise Exception(
                "Manifest does not contain DestinyInventoryItemDefinition")

        self.db_items = manifest['DestinyInventoryItemDefinition']
        # build search "index"
        self.logger.info("Creating lookup index")
        self.lookup_items = {}
        for item in self.db_items.values():
            self.lookup_items[item['hash']
                              ] = item['displayProperties']['name'].lower()
        self.logger.info("Done")

    def get_info(self, query, mod):
        self.ratelimit(mod)
        query = query.lower()
        self.logger.debug("Getting info for %s" % query)

        results = {}
        for key, name in self.lookup_items.items():
            if query in name:
                results[key] = SequenceMatcher(None, name, query).ratio()
                cats = self.db_items[str(key)]['itemCategoryHashes']
                if 1 in cats or 20 in cats:
                    results[key] *= 2

        return sorted(results.items(), key=lambda t: t[1], reverse=True)[:3]

    def ratelimit(self, mod=False):
        if mod:
            return

        if (self.lastquery - (time.time())) > 1:
            self.lastquery = time.time()
            self.queryCount = 1
        elif self.queryCount < self.ratelimitPerSec:
            self.queryCount += 1
        else:
            self.logger.info("ratelimit exceeded")
            while self.lastquery + 1 > (time.time()):
                continue
            self.lastquery = time.time()
            self.queryCount = 1

    # responses is a list of sorted (hash, match%) tuples
    def compile_response(self, responses):
        comment = "Name | Type | Description | More&#160;Info\n-|-|-|-"
        seen = []
        for r in responses:
            # currently just a single table with up to three entries per [[query]], not separated. might change later though :)
            for t in r:
                i = t[0]
                if not i or i in seen or str(i) not in self.db_items:
                    continue
                seen.append(i)
                info = self.db_items[str(i)]

                name = "[{}](https://www.bungie.net{})".format(info['displayProperties']
                                                               ['name'], info['displayProperties']['icon'])

                moreInfoData = "[More&#160;Info]({}{})".format(
                    self.linkprefix, info['hash'])

                Description = info['displayProperties']['description'].replace(
                    "\n", "")

                comment += "\n{}|{}|{}|{}".format(
                    name, info['itemTypeDisplayName'], Description, moreInfoData)

        return comment + "\n\n---\n\nThis lovely bot is a service and a luxury. Please do not abuse him, or we will have to limit your use of this bot."

    def get_all_asks(self, body, mod=False):
        asks = []
        asked = []
        for q in re.findall(self.matchstring, body):
            if q in asked:
                continue
            asked.append(q)
            asks.append(self.get_info(q, mod))
        return asks[:10]

    def action(self):
        for comment in self.subreddit.stream.comments():
            if comment.id in self.done:
                continue
            self.done.append(comment.id)
            if comment.created_utc < self.starttime:
                continue
            if comment.author in self.mods:
                items = self.get_all_asks(comment.body, mod=True)
            else:
                items = self.get_all_asks(comment.body)
            if any(items):
                response = self.compile_response(items)
                try:
                    comment.reply(response)
                    self.logger.info("Replied to comment by %s, id - %s" %
                                     (str(comment.author), comment.fullname))
                except praw.exceptions.APIException:
                    self.logger.warning(
                        "Comment Deleted, id - %s" % str(comment.fullname))

    def launch(self):
        while self.canLaunch:
            try:
                self.action()
                self.logger.debug("Sleeping for 60 seconds.")
                time.sleep(60)

            except UnicodeEncodeError as e:
                self.logger.error(
                    "Caught UnicodeEncodeError! {}".format(str(e)))

            except prawcore.exceptions.InvalidToken:
                self.logger.warn(
                    'API Token Error. Likely on reddits end. Issue self-resolves.')
                time.sleep(180)
            except prawcore.exceptions.BadJSON:
                self.logger.warning(
                    'PRAW didn\'t get good JSON, probably reddit sending bad data due to site issues.')
                time.sleep(180)
            except (prawcore.exceptions.ResponseException,
                    prawcore.exceptions.RequestException,
                    prawcore.exceptions.ServerError,
                    urllib3.exceptions.TimeoutError,
                    Timeout):
                self.logger.warning(
                    'HTTP Requests Error. Likely on reddits end due to site issues.')
                time.sleep(300)
            except praw.exceptions.APIException:
                self.logger.error('PRAW/Reddit API Error')
                time.sleep(30)
            except praw.exceptions.ClientException:
                self.logger.error('PRAW Client Error')
                time.sleep(30)
            except KeyboardInterrupt:
                self.logger.warning('Caught KeyboardInterrupt - Exiting')
                sys.exit()
            except Exception:
                self.logger.exception('General Exception - Sleeping 5 min')
                time.sleep(300)

        self.logger.info("Exiting")
        sys.exit()


def main():
    callresponsebot = CallResponse()
    callresponsebot.launch()
